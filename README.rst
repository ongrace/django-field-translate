=====
Field Translate
=====

Field Translate

Quick start
-----------

1. Add "location" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'field_translate',
    ]

2. Example usage::

    to cansado.

3. Start the development server and visit http://127.0.0.1:8000/admin/
   to visit the example form (you'll need the Admin app enabled).